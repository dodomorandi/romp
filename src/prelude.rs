//! A "prelude" for users of the `romp` crate.
//!
//! This prelude is similar to the standard library's prelude in that you'll
//! almost always want to import its entire contents, but unlike the standard
//! library's prelude you'll have to do so manually:
//!
//! ```
//! use romp::prelude::*;
//! ```
//!

pub use crate::message::stomp_message::StompCommand;
pub use crate::message::stomp_message::StompMessage;
pub use crate::session::stomp_session::StompSession;
pub use crate::workflow::context::Context;
pub use crate::workflow::filter::{FilterError, MessageFilter};
pub use crate::workflow::filter::{HttpError, HttpFilter};
pub use crate::workflow::http_router::insert_http as romp_http;
pub use crate::workflow::http_router::insert_http_filter as romp_http_filter;
pub use crate::workflow::http_router::insert_http_filter_at as romp_http_filter_at;
pub use crate::workflow::internal::{romp_pub_message, romp_subscribe};
pub use crate::workflow::router::insert_stomp as romp_stomp;
pub use crate::workflow::router::insert_stomp_filter as romp_stomp_filter;
pub use crate::workflow::router::insert_stomp_filter_at as romp_stomp_filter_at;
pub use crate::workflow::{CONTINUE, HANDLED};

pub use crate::bootstrap::romp_bootstrap;
