//! Code related to parsing STOMP message bodies, either fixed `content-length` or `\0` terminated.

use crate::message::stomp_message::StompMessage;

// looks for body_parser/fixed_length.rs
pub mod fixed_length;
pub mod text;

#[derive(Debug, PartialEq)]
pub enum ParserState {
    /// finished reading the whole message
    Done,
    /// awaiting more input
    Again,
    /// tool long
    BodyFlup,
    /// missing trailing \0
    InvalidMessage,
}

pub trait BodyParser {
    fn push(&mut self, buffer: &[u8], message: &mut StompMessage) -> Result<usize, ParserState>;

    fn is_done(&self) -> bool;
}
