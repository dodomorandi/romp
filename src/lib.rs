//! # Romp
//!
//! `romp` is a messaging server that uses the STOMP protocol, and STOMP over WebSockets.
//!
//! `romp` can run as a standalone server, hosting queues and/or topics to which clients can subscribe or
//! send messages.   It can also host bespoke filters (handlers) to programmatically respond to incoming messages.
//! This enables romp servers to act as app servers, using asynchronous messaging model.
//!
//!
//! Custom filters can be added as follow...
//!
//! ```
//!
//!     // Filter structs have an init() method
//!     use romp::bootstrap::romp_bootstrap;
//!     use romp::workflow::filter::ping::PingFilter;
//!     use romp::prelude::{romp_stomp_filter, romp_http_filter, romp_stomp, romp_http};
//!     use romp::workflow::filter::http::hello::HelloFilter;
//!     use romp::message::response::{get_response_ok, get_http_ok_msg};
//!     use std::sync::Arc;
//!
//!     romp_stomp_filter(Box::new(PingFilter {}));
//!
//!     // Http Filter structs different error handling
//!     romp_http_filter(Box::new(HelloFilter {}));
//!
//!     // STOMP filters can be written as closures
//!     romp_stomp(|ctx, msg| {
//!         if let Some(hdr) = msg.get_header("special-header") {
//!             let mut session = ctx.session.write().unwrap();
//!             session.send_message(get_response_ok("howdy".to_string()));
//!             return Ok(true);
//!         }
//!         Ok(false)
//!     });
//!
//!     // as can HTTP filters
//!     romp_http(|ctx, msg| {
//!         if "/pingerooni".eq(ctx.attributes.get("romp.uri").unwrap()) {
//!             let mut session = ctx.session.write().unwrap();
//!             session.send_message(Arc::new(get_http_ok_msg()));
//!             return Ok(true);
//!         }
//!         Ok(false)
//!     });
//!
//!     // Then bootstrap the server, this is commented out in the docs, because of some bug in the cargo test when this comment is uncommented.
//!     // There is no bug in the code below its used like that in main.rs that compiles.
//!     // tokio::run(bootstrap_romp());
//!
//! ```

#[macro_use]
extern crate lazy_static;

pub mod body_parser;
pub mod bootstrap;
pub mod config;
pub mod downstream;
pub mod errors;
pub mod init;
pub mod message;
pub mod parser;
pub mod persist;
pub mod prelude;
pub mod session;
#[cfg(unix)]
pub mod system;
pub mod util;
pub mod web_socket;
pub mod workflow;
