//! Workflow concerns application logic, either in built STOMP message handling, admin functions
//! or bespoke application specific functions.

pub mod admin_router;
pub mod console;
pub mod context;
pub mod destination;
pub mod filter;
pub mod http_router;
pub mod internal;
pub mod router;
pub mod sha_auth;

/// Workflow/filter handled the request not further processing needed.
pub const HANDLED: bool = true;
/// Workflow/filter indicates the filter chain should continue with the next filter.
pub const CONTINUE: bool = false;
