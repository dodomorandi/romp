use log::*;

use crate::message::stomp_message::StompMessage;
use crate::workflow::context::Context;
use crate::workflow::filter::admin::shutdown::ShutdownFilter;
use crate::workflow::filter::{AdminFilter, FilterError};

// route /virt/admin/ SEND messages through the application

lazy_static! {
    pub static ref FILTER_CHAIN_STATIC: FilterChain = FilterChain {
        chain: build_filter_chain()
    };
}

pub struct FilterChain {
    chain: Vec<Box<dyn AdminFilter + Sync>>,
}

pub fn build_filter_chain() -> Vec<Box<dyn AdminFilter + Sync>> {
    let mut route: Vec<Box<dyn AdminFilter + Sync>> = vec![Box::new(ShutdownFilter::new())];

    for filter in route.iter_mut() {
        filter.init();
    }

    debug!("init finished");
    route
}

/// route incoming messages
pub fn route_admin(mut ctx: &mut Context, message: &mut StompMessage) -> Result<bool, FilterError> {
    for f in &FILTER_CHAIN_STATIC.chain {
        match f.do_filter(&mut ctx, message) {
            Ok(false) => {} // loop
            Ok(true) => return Ok(true),
            Err(fe) => return Err(fe),
        }
    }

    Ok(false)
}
