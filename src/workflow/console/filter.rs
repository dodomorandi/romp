use std::sync::Arc;

use crate::prelude::*;

use crate::init::NGIN;

/// handles HTTP requests for `xtomp-console`.

#[derive(Debug, Default)]
pub struct ConsoleFilter {}

impl ConsoleFilter {
    pub fn new() -> ConsoleFilter {
        ConsoleFilter {}
    }
}

impl HttpFilter for ConsoleFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        context: &mut Context,
        _message: &mut StompMessage,
    ) -> Result<bool, HttpError> {
        let uri = context.attributes.get("romp.uri").unwrap().trim();
        if uri.eq("/romp/ngin-config/") {
            let mut session = context.session.write().unwrap();
            session.send_message(Arc::new(NGIN.response_ngin_config()));
            session.shutdown();
            return Ok(HANDLED);
        }
        if uri.eq("/romp/ngin-info/") {
            let mut session = context.session.write().unwrap();
            session.send_message(Arc::new(NGIN.response_ngin_info()));
            session.shutdown();
            return Ok(HANDLED);
        }
        if uri.starts_with("/romp/destination-info/?destination=") {
            // TODO be a bit more forgiving with URL syntax
            let destination = uri.split('=').nth(1).unwrap();

            let mut session = context.session.write().unwrap();
            session.send_message(Arc::new(NGIN.response_destination_info(destination)));
            session.shutdown();
            return Ok(HANDLED);
        }

        Ok(CONTINUE)
    }
}
