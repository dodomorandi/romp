//! Internal API contains methods that are only useful in the context of bespoke filters.

use crate::errors::ServerError;
use crate::init::SERVER;
use crate::message::response::get_http_ok_text_content;
use crate::message::stomp_message::StompMessage;
use crate::session::stomp_session::StompSession;
use crate::session::subscription::{Ack, Subscription};
use crate::workflow::context::Context;
use std::sync::{Arc, RwLock};

/// internal API

/// publish a message to a destination
pub fn romp_pub_message(destination: &str, message: StompMessage) -> Result<usize, ServerError> {
    return match SERVER.find_destination(destination) {
        Some(destination) => destination.write().unwrap().push(&message),
        _ => Err(ServerError::DestinationUnknown),
    };
}

pub fn romp_subscribe(destination: &str) -> Result<Arc<RwLock<StompSession>>, ServerError> {
    return match SERVER.find_destination(destination) {
        Some(destination) => {
            let session = StompSession::new();
            let arc_session = Arc::new(RwLock::new(session));
            let sub = Arc::new(RwLock::new(Subscription::new(
                arc_session.clone(),
                destination.clone(),
                0,
                Ack::Auto,
                None,
                false,
            )));
            let mut dest = destination.write().unwrap();
            match dest.subscribe(sub) {
                Err(e) => Err(e),
                Ok(_) => Ok(arc_session),
            }
        }
        _ => Err(ServerError::DestinationUnknown),
    };
}

pub fn romp_http_response(context: Context, text: String, content_type: &'static str) {
    if let Ok(session) = context.session.read() {
        let message = get_http_ok_text_content(text, content_type);
        session.send_message(message);
    }
}
