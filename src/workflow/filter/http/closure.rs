use crate::prelude::*;

use core::fmt;

/// Generic filter for HTTP messages

pub struct ClosureFilter {
    closure: fn(&mut Context, message: &mut StompMessage) -> Result<bool, HttpError>,
}

impl ClosureFilter {
    pub fn new(
        closure: fn(&mut Context, message: &mut StompMessage) -> Result<bool, HttpError>,
    ) -> ClosureFilter {
        ClosureFilter { closure }
    }
}

impl HttpFilter for ClosureFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, HttpError> {
        (self.closure)(context, message)
    }
}

impl fmt::Debug for ClosureFilter {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ClosureFilter")
    }
}
