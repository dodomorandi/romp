use std::sync::Arc;

use crate::message::response::get_http_404_msg;
use crate::message::stomp_message::StompMessage;
use crate::workflow::context::Context;
use crate::workflow::filter::{HttpError, HttpFilter};

/// handles previously unhandled URIs, sends 404
///
#[derive(Debug, Default)]
pub struct NotFoundFilter {}

impl NotFoundFilter {
    pub fn new() -> NotFoundFilter {
        NotFoundFilter {}
    }
}

impl HttpFilter for NotFoundFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        context: &mut Context,
        _message: &mut StompMessage,
    ) -> Result<bool, HttpError> {
        context
            .session
            .read()
            .unwrap()
            .send_message(Arc::new(get_http_404_msg()));
        Err(HttpError {
            // get("romp.uri").unwrap() is safe because GetFilter creates it or prevents this filter running
            log_message: Some(format!(
                "404: {}",
                context.attributes.get("romp.uri").unwrap()
            )),
        })
    }
}
