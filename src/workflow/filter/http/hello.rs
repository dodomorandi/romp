use std::sync::Arc;

use crate::prelude::*;

use crate::message::response::get_http_ok_msg;

/// handles /romp/hello uri
/// This is an example http filter to be loaded dynamically its not part of the default route.
///
#[derive(Debug, Default)]
pub struct HelloFilter {}

impl HelloFilter {
    pub fn new() -> HelloFilter {
        HelloFilter {}
    }
}

impl HttpFilter for HelloFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        context: &mut Context,
        _message: &mut StompMessage,
    ) -> Result<bool, HttpError> {
        if let Some(uri) = context.attributes.get("romp.uri") {
            if uri.trim().eq("/romp/hello") {
                let mut session = context.session.write().unwrap();
                session.send_message(Arc::new(get_http_ok_msg()));
                session.shutdown();
                return Ok(HANDLED);
            }
        }

        Ok(CONTINUE)
    }
}
