use std::sync::Arc;

use crate::prelude::*;

use crate::message::response::get_http_ok_msg;

/// handles /romp/health_check uri
///
#[derive(Debug, Default)]
pub struct HealthCheckFilter {}

impl HealthCheckFilter {
    pub fn new() -> HealthCheckFilter {
        HealthCheckFilter {}
    }
}

impl HttpFilter for HealthCheckFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        context: &mut Context,
        _message: &mut StompMessage,
    ) -> Result<bool, HttpError> {
        if context
            .attributes
            .get("romp.uri")
            .unwrap()
            .trim()
            .eq("/romp/health_check")
        {
            let mut session = context.session.write().unwrap();
            session.send_message(Arc::new(get_http_ok_msg()));
            session.shutdown();
            return Ok(HANDLED);
        }

        Ok(CONTINUE)
    }
}
