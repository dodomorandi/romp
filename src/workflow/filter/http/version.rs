use std::sync::Arc;

use crate::prelude::*;

use crate::message::response::get_http_client_err_msg;
use crate::util::some_string;
use crate::web_socket::ws_headers::ws_validate_hdr_host;

/// Checks we are HTTP/1.1
///
#[derive(Debug, Default)]
pub struct VersionFilter {}

impl VersionFilter {
    pub fn new() -> VersionFilter {
        VersionFilter {}
    }
}

impl HttpFilter for VersionFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, HttpError> {
        // Technically we should request-line for HTTP/1.1
        if !ws_validate_hdr_host(message) {
            // not HTTP/1.1
            let session = context.session.read().unwrap();
            session.send_message(Arc::new(get_http_client_err_msg()));
            return Err(HttpError {
                log_message: some_string("missing host header"),
            });
        }

        Ok(CONTINUE)
    }
}
