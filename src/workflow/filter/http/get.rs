use std::sync::Arc;

use crate::message::response::get_http_err_msg;
use crate::message::stomp_message::StompMessage;
use crate::web_socket::ws_headers::ws_parse_request_line;
use crate::workflow::context::Context;
use crate::workflow::filter::{HttpError, HttpFilter};
use crate::workflow::{CONTINUE, HANDLED};

/// handles GET method.
///
/// Subsequent filters may presume `context.attributes.get("romp.uri")` exists.
///
#[derive(Debug, Default)]
pub struct GetFilter {}

impl GetFilter {
    pub fn new() -> GetFilter {
        GetFilter {}
    }
}

impl HttpFilter for GetFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, HttpError> {
        return match ws_parse_request_line(message) {
            Ok(uri) => {
                context.attributes.insert("romp.uri", uri);
                Ok(CONTINUE)
            }
            Err(_) => {
                context
                    .session
                    .read()
                    .unwrap()
                    .send_message(Arc::new(get_http_err_msg()));
                Ok(HANDLED)
            }
        };
    }
}
