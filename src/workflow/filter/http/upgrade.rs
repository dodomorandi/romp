use std::sync::Arc;

use log::*;

use crate::init::CONFIG;
use crate::message::response::{get_http_client_err_msg, get_http_err_msg, get_http_upgrade_msg};
use crate::prelude::*;
use crate::util::some_string;
use crate::web_socket::ws_headers::{ws_get_websocket_accept_key, ws_validate_hdrs};

/// handles HTTP Upgrade
///
#[derive(Debug, Default)]
pub struct UpgradeFilter {}

impl UpgradeFilter {
    pub fn new() -> UpgradeFilter {
        UpgradeFilter {}
    }
}

impl HttpFilter for UpgradeFilter {
    fn init(&mut self) {}

    /// returns true if the message was fully handled and the chain should stop
    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, HttpError> {
        if message.get_header_case_insensitive("Upgrade").is_some() {
            if !CONFIG.websockets {
                context
                    .session
                    .read()
                    .unwrap()
                    .send_message(Arc::new(get_http_client_err_msg()));
                return Err(HttpError {
                    log_message: some_string("http upgrade forbidden"),
                });
            }
            return match ws_validate_hdrs(message) {
                Ok(_) => {
                    {
                        context.session.write().unwrap().ws_upgrade();
                    }
                    context
                        .session
                        .read()
                        .unwrap()
                        .send_message(Arc::new(get_http_upgrade_msg(
                            &ws_get_websocket_accept_key(message),
                        )));
                    debug!("upgrade success");
                    Ok(HANDLED)
                }
                Err(upgrade_error) => {
                    debug!("upgrade_error={:?}", upgrade_error);
                    // TODO WsUpgradeError::OriginDenied could be visible in the client
                    context
                        .session
                        .read()
                        .unwrap()
                        .send_message(Arc::new(get_http_err_msg()));
                    Err(HttpError {
                        log_message: some_string("http upgrade error"),
                    })
                }
            };
        } else {
            // if we got Connection:Upgrade but not Upgrade:websocket, handled above, we should also fail fast
            if let Some(connection) = message.get_header_case_insensitive("Connection") {
                if connection.eq_ignore_ascii_case("Upgrade") {
                    context
                        .session
                        .read()
                        .unwrap()
                        .send_message(Arc::new(get_http_err_msg()));
                    return Err(HttpError {
                        log_message: some_string("http upgrade error"),
                    });
                }
            }
        }

        Ok(CONTINUE)
    }
}
