use log::*;

use crate::prelude::*;

use crate::errors::ServerError;
use crate::init::SERVER;
use crate::util::some_string;

/// handles the MESSAGE STOMP messages type from upstream servers, by publishing them locally
/// this enables bridging multiple downstreams in a single upstream
#[derive(Debug, Default)]
pub struct ResendFilter {}

impl ResendFilter {
    pub fn new() -> ResendFilter {
        ResendFilter {}
    }
}

impl MessageFilter for ResendFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, FilterError> {
        if matches!(message.command, StompCommand::Message) {
            if !context.is_downstream() {
                return Ok(CONTINUE);
            }

            match message.get_header("destination") {
                Some(destination_name) => {
                    if let Some(reply_destination) = destination_name.strip_suffix("/up") {
                        let destination = SERVER.find_destination(reply_destination);

                        match destination {
                            // this is a config error, no need to report this to downstream
                            None => {
                                return Err(FilterError {
                                    reply_message: None,
                                    log_message: some_string("destination unknown"),
                                    fatal: false,
                                })
                            }
                            Some(destination) => {
                                {
                                    let destination = destination.read().unwrap();
                                    debug!(
                                        "pushing msg to dest {} len={}",
                                        destination.name(),
                                        destination.len()
                                    );
                                }
                                // publish the message
                                return match destination.write().unwrap().push_resend(message) {
                                    Ok(_) => Ok(CONTINUE),
                                    Err(ServerError::DestinationFlup) => Err(FilterError {
                                        reply_message: some_string("destination flup"),
                                        log_message: some_string("destination flup"),
                                        fatal: false,
                                    }),
                                    Err(_) => Err(FilterError {
                                        reply_message: some_string("general"),
                                        log_message: some_string(
                                            "general error pushing to destination",
                                        ),
                                        fatal: true,
                                    }),
                                };
                            }
                        }
                    }
                    // else who is sending us this??
                }
                None => {
                    return Err(FilterError {
                        // should not happen, syntax error by upstream
                        reply_message: None,
                        log_message: some_string("missing destination header"),
                        fatal: false,
                    });
                }
            }
        }

        Ok(CONTINUE)
    }
}
