use log::*;

use crate::prelude::*;

use crate::errors::ServerError;
use crate::init::SERVER;
use crate::session::stomp_session::FLAG_WEB;
use crate::util::some_string;

/// handles the SEND STOMP message
///
#[derive(Debug, Default)]
pub struct SendFilter {}

impl SendFilter {
    pub fn new() -> SendFilter {
        SendFilter {}
    }
}

impl MessageFilter for SendFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, FilterError> {
        if matches!(message.command, StompCommand::Send) {
            match message.get_header("destination") {
                Some(destination_name) => {
                    // Virtual destinations are not forwarded to a real destination
                    // its expected that a filter up the chain handles these messages
                    if destination_name.starts_with("/virt/") {
                        return Ok(CONTINUE);
                    }

                    let destination = SERVER.find_destination(destination_name);

                    match destination {
                        None => {
                            return Err(FilterError {
                                reply_message: some_string("destination unknown"),
                                log_message: some_string("destination unknown"),
                                fatal: false,
                            })
                        }
                        Some(destination) => {
                            {
                                let destination = destination.read().unwrap();
                                debug!(
                                    "pushing msg to dest {} len={}",
                                    destination.name(),
                                    destination.len()
                                );

                                // security
                                if destination.write_block() {
                                    return Err(FilterError::setup_filter_error(
                                        "blocked",
                                        "dest write blocked",
                                        false,
                                    ));
                                }

                                if destination.web_write_block()
                                    && context.session.read().unwrap().get_flag(FLAG_WEB)
                                {
                                    return Err(FilterError::setup_filter_error(
                                        "blocked",
                                        "dest web write blocked",
                                        false,
                                    ));
                                }
                            }
                            // publish the message
                            return match destination.write().unwrap().push(message) {
                                Ok(_) => Ok(CONTINUE),
                                Err(ServerError::DestinationFlup) => {
                                    Err(FilterError::setup_filter_error(
                                        "destination flup",
                                        "destination flup",
                                        false,
                                    ))
                                }
                                Err(_) => Err(FilterError::setup_filter_error(
                                    "general",
                                    "general error pushing to destination",
                                    true,
                                )),
                            };
                        }
                    }
                }
                None => {
                    return Err(FilterError::setup_filter_error(
                        "syntax",
                        "missing destination header",
                        true,
                    ))
                }
            }
        }

        Ok(CONTINUE)
    }
}
