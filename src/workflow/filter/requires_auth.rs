use crate::prelude::*;

use crate::init::CONFIG;
use crate::util::some_string;

/// ensures that filter chain is interrupted it a user is not logged in and sends messages that require login
/// also filters out some invalid workflow, such as client sending ERROR or MESSAGE that are server responses.
#[derive(Debug)]
pub struct RequiresAuthFilter {
    requires_auth: bool,
}

impl RequiresAuthFilter {
    pub fn new() -> RequiresAuthFilter {
        RequiresAuthFilter::default()
    }
}

impl Default for RequiresAuthFilter {
    fn default() -> RequiresAuthFilter {
        RequiresAuthFilter {
            requires_auth: false,
        }
    }
}

impl MessageFilter for RequiresAuthFilter {
    fn init(&mut self) {
        self.requires_auth = CONFIG.requires_auth();
    }

    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, FilterError> {
        if !self.requires_auth {
            return Ok(CONTINUE);
        }
        let is_downstream = context.is_downstream();

        return match message.command {
            StompCommand::Stomp
            | StompCommand::Connect
            | StompCommand::Disconnect
            | StompCommand::Get => {
                // no login required
                Ok(CONTINUE)
            }
            StompCommand::Unsubscribe
            | StompCommand::Subscribe
            | StompCommand::Ack
            | StompCommand::Nack
            | StompCommand::Begin
            | StompCommand::Commit
            | StompCommand::Abort => {
                if !context.session.read().unwrap().is_authenticated() {
                    return Err(FilterError {
                        reply_message: some_string("denied"),
                        log_message: some_string("denied"),
                        fatal: false,
                    });
                }
                Ok(CONTINUE)
            }
            StompCommand::Send => {
                if context.session.read().unwrap().is_authenticated() || is_downstream {
                    return Ok(CONTINUE);
                }
                return Err(FilterError {
                    reply_message: some_string("denied"),
                    log_message: some_string("denied"),
                    fatal: false,
                });
            }
            StompCommand::Unknown | StompCommand::Init => Err(FilterError {
                reply_message: None,
                log_message: some_string("filtering uninitialised message?"),
                fatal: true,
            }),
            StompCommand::Error
            | StompCommand::Message
            | StompCommand::Connected
            | StompCommand::Receipt => {
                if is_downstream {
                    return Ok(CONTINUE);
                }
                Err(FilterError {
                    reply_message: None,
                    log_message: some_string("filtering reply message?"),
                    fatal: true,
                })
            }
            StompCommand::Ok
            | StompCommand::Upgrade
            | StompCommand::ServerError
            | StompCommand::ServiceUnavailable
            | StompCommand::ClientError
            | StompCommand::NotFound => Err(FilterError {
                reply_message: None,
                log_message: some_string("filtering reply message?"),
                fatal: true,
            }),
        };
    }
}
