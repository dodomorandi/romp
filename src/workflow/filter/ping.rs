use crate::prelude::*;

use crate::util::some_string;

/// handles the SEND STOMP message with a ping header
/// this is and example filter that can be dynamically loaded, its not part of the default route.
///
#[derive(Debug, Default)]
pub struct PingFilter {}

impl PingFilter {
    pub fn new() -> PingFilter {
        PingFilter {}
    }
}

impl MessageFilter for PingFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        _context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, FilterError> {
        if message.command == StompCommand::Send {
            if let Some(hdr) = message.get_header("ping") {
                return Err(FilterError {
                    reply_message: Some(hdr.to_string()),
                    log_message: some_string("ping"),
                    fatal: false,
                });
            }
        }

        Ok(CONTINUE)
    }
}
