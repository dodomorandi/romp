use crate::prelude::*;

use crate::util::some_string;

/// handles the ACK STOMP message
///
/// Subscriptions can decide if they want to auto-ack or to send manual acks, destinations can force
/// auto-ack
///
#[derive(Debug, Default)]
pub struct AckFilter {}

impl AckFilter {
    pub fn new() -> AckFilter {
        AckFilter {}
    }
}

impl MessageFilter for AckFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, FilterError> {
        match message.command {
            StompCommand::Ack | StompCommand::Nack => {
                if let Some(message_id) = message.get_header("message-id") {
                    let ack_nack = message.command == StompCommand::Ack;
                    return match context.session.read().unwrap().ack(ack_nack, message_id) {
                        true => Ok(CONTINUE),
                        false => Err(FilterError {
                            reply_message: some_string("ack err"),
                            log_message: some_string("ack err"),
                            fatal: false,
                        }),
                    };
                }
            }
            _ => {}
        }

        Ok(CONTINUE)
    }
}
