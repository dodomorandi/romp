use crate::prelude::*;

use std::fmt;

/// generic filter for STOMP messages.

pub struct ClosureFilter {
    closure: fn(&mut Context, message: &mut StompMessage) -> Result<bool, FilterError>,
}

impl ClosureFilter {
    pub fn new(
        closure: fn(&mut Context, message: &mut StompMessage) -> Result<bool, FilterError>,
    ) -> ClosureFilter {
        ClosureFilter { closure }
    }
}

impl MessageFilter for ClosureFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, FilterError> {
        (self.closure)(context, message)
    }
}

// use this when #[derive(Debug)] is not possible
impl fmt::Debug for ClosureFilter {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ClosureFilter")
    }
}
