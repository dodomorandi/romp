use log::*;

use crate::prelude::*;

use crate::init::CONFIG;

/// handles logging messages with log4rs
///
#[derive(Debug, Default)]
pub struct LogFilter {}

impl LogFilter {
    pub fn new() -> LogFilter {
        LogFilter {}
    }
}

impl MessageFilter for LogFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, FilterError> {
        match CONFIG.access_log {
            LevelFilter::Debug => {
                let session = context.session.read().unwrap();
                debug!("received {:?} usr={}", message.as_string(), session.user());
            }
            LevelFilter::Info => match &message.command {
                StompCommand::Connect | StompCommand::Stomp => {
                    info!("CONNECT");
                }
                StompCommand::Subscribe => {
                    let session = context.session.read().unwrap();
                    info!(
                        "SUBSCRIBE q={} usr={}",
                        message
                            .get_header("destination")
                            .unwrap_or(&"unknown".to_string()),
                        session.user()
                    );
                }
                StompCommand::Send | StompCommand::Ack | StompCommand::Nack => {
                    let session = context.session.read().unwrap();
                    info!("{} usr={}", message.command.as_string(), session.user());
                }
                _ => {}
            },
            LevelFilter::Warn => {
                if matches!(message.command, StompCommand::Nack) {
                    let session = context.session.read().unwrap();
                    warn!("Nack usr={}", session.user());
                }
            }
            _ => {}
        }

        Ok(CONTINUE)
    }
}
