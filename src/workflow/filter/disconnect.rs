use crate::prelude::*;

/// handles the DISCONNECT STOMP message
///
#[derive(Debug, Default)]
pub struct DisconnectFilter {}

impl DisconnectFilter {
    pub fn new() -> DisconnectFilter {
        DisconnectFilter {}
    }
}

impl MessageFilter for DisconnectFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, FilterError> {
        if StompCommand::Disconnect == message.command {
            context.session.write().unwrap().shutdown();
        }

        Ok(HANDLED)
    }
}
