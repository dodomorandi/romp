use std::sync::{Arc, RwLock};

use log::*;

use crate::prelude::*;

use crate::errors::ServerError;
use crate::init::CONFIG;
use crate::init::SERVER;
use crate::session::filter::Filter;
use crate::session::stomp_session::FLAG_WEB;
use crate::session::subscription::{parse_ack_hdr, Ack, Subscription};
use crate::util::some_string;

/// handles the SUBSCRIBE STOMP message, by hooking up this session to the destination
///
#[derive(Debug, Default)]
pub struct SubscribeFilter {}

impl SubscribeFilter {
    pub fn new() -> SubscribeFilter {
        SubscribeFilter {}
    }
}

impl MessageFilter for SubscribeFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, FilterError> {
        if matches!(message.command, StompCommand::Subscribe) {
            if context.session.read().unwrap().count_subscription() == CONFIG.max_subs {
                return Err(FilterError {
                    reply_message: some_string("subs flup"),
                    log_message: some_string("subs flup"),
                    fatal: false,
                });
            }

            match message.get_header("destination") {
                Some(d) => {
                    let d = SERVER.find_destination(d);
                    match d {
                        None => {
                            return Err(FilterError {
                                reply_message: some_string("destination unknown"),
                                log_message: some_string("destination unknown"),
                                fatal: false,
                            })
                        }
                        Some(d) => {
                            // security
                            {
                                let destination = d.read().unwrap();
                                if destination.read_block() {
                                    return Err(FilterError {
                                        reply_message: some_string("blocked"),
                                        log_message: some_string("dest read blocked"),
                                        fatal: false,
                                    });
                                }
                                if destination.web_read_block()
                                    && context.session.read().unwrap().get_flag(FLAG_WEB)
                                {
                                    return Err(FilterError {
                                        reply_message: some_string("blocked"),
                                        log_message: some_string("dest web read blocked"),
                                        fatal: false,
                                    });
                                }
                            }

                            // Subscription filtering
                            let mut filter = None;
                            if let Some(filter_hdr) = message.get_header("filter") {
                                if let Ok(f) = Filter::parse(filter_hdr) {
                                    filter = Some(f);
                                } else {
                                    return Err(FilterError {
                                        reply_message: some_string("syntax"),
                                        log_message: some_string("filter parse error"),
                                        fatal: true,
                                    });
                                }
                            }

                            // Acknowledge mode
                            let mut ack_mode;
                            if let Some(ack_hdr) = message.get_header("ack") {
                                ack_mode = parse_ack_hdr(ack_hdr);
                            } else {
                                ack_mode = Ack::Auto;
                            }

                            let filter_self;
                            let result;
                            let subscription;
                            {
                                let mut destination = d.write().unwrap();
                                if destination.auto_ack() {
                                    ack_mode = Ack::Auto;
                                }
                                filter_self = destination.filter_self();
                                subscription = Arc::new(RwLock::new(Subscription::new(
                                    context.session.clone(),
                                    d.clone(),
                                    0,
                                    ack_mode,
                                    filter,
                                    filter_self,
                                )));
                                result = destination.subscribe(subscription.clone());
                            }
                            match result {
                                Err(ServerError::SubscriptionFlup) => {
                                    context
                                        .attributes
                                        .insert("cancel-receipt", String::from("true"));
                                    return Err(FilterError {
                                        reply_message: some_string("subs flup"),
                                        log_message: some_string("subs flup"),
                                        fatal: false,
                                    });
                                }
                                // shutting down
                                Err(_) => {
                                    context
                                        .attributes
                                        .insert("cancel-receipt", String::from("true"));
                                    return Err(FilterError {
                                        reply_message: some_string("general"),
                                        log_message: None,
                                        fatal: true,
                                    });
                                }
                                Ok(_) => {
                                    context
                                        .session
                                        .write()
                                        .unwrap()
                                        .add_subscription(subscription);
                                    debug!("subscribed successfully");
                                    return Ok(CONTINUE);
                                }
                            }
                        }
                    }
                }
                None => {
                    return Err(FilterError {
                        reply_message: some_string("syntax"),
                        log_message: some_string("missing destination header"),
                        fatal: false,
                    })
                }
            }
        }

        Ok(CONTINUE)
    }
}
