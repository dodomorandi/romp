use std::sync::{Arc, RwLock};

use crate::prelude::*;

use crate::init::CONFIG;
use crate::init::USER;
use crate::message::response::{get_response_connected, get_response_error};
use crate::session::ecg;
use crate::session::stomp_session::FLAG_ADMIN;
use crate::util::some_string;
use crate::workflow::sha_auth::sha_auth;

/// handles the CONNECT STOMP message, and its synonym "STOMP"
///
///
#[derive(Debug)]
pub struct ConnectFilter {
    /// needs some kind of authentication
    requires_auth: bool,
    /// supports login/passcode from romp.toml
    supports_simple_auth: bool,
    /// supports conf/user file
    supports_user_auth: bool,
    /// requires sha auth (not compatibel with outher types
    requires_sha_auth: bool,
}

impl Default for ConnectFilter {
    fn default() -> Self {
        Self {
            requires_auth: false,
            supports_simple_auth: false,
            supports_user_auth: false,
            requires_sha_auth: false,
        }
    }
}

impl ConnectFilter {
    pub fn new() -> Self {
        Self::default()
    }

    /// Get a CONNECTED response message with the heart-beat header added
    fn get_response_connected(
        &self,
        message: &StompMessage,
        session: Arc<RwLock<StompSession>>,
    ) -> Arc<StompMessage> {
        if let Some(hdr) = ecg::get_request_header(message) {
            if let Err(message) = ecg::parse_header(hdr, &session) {
                session.write().unwrap().shutdown();
                return get_response_error(message);
            }
        }
        get_response_connected(ecg::get_response_header(&session))
    }
}

impl MessageFilter for ConnectFilter {
    fn init(&mut self) {
        self.requires_auth = CONFIG.requires_auth();
        self.requires_sha_auth = CONFIG.requires_sha_auth();
        self.supports_simple_auth = CONFIG.supports_simple_auth();
        self.supports_user_auth = CONFIG.supports_user_auth();
    }

    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, FilterError> {
        match message.command {
            StompCommand::Stomp | StompCommand::Connect => {
                if self.requires_auth {
                    let login = message.get_header("login");
                    let passcode = message.get_header("passcode");

                    if let (Some(login), Some(passcode)) = (login, passcode) {
                        if self.requires_sha_auth {
                            let secret = &CONFIG.secret.as_ref();
                            if let Ok(username) = sha_auth(
                                login,
                                passcode,
                                secret.unwrap(),
                                CONFIG.secret_timeout,
                                context.now().timestamp_millis(),
                            ) {
                                {
                                    let mut session = context.session.write().unwrap();
                                    session.set_login(username.as_str());
                                }
                                let connected_message =
                                    self.get_response_connected(message, context.session.clone());
                                context
                                    .session
                                    .read()
                                    .unwrap()
                                    .send_message(connected_message);

                                return Ok(HANDLED);
                            } else {
                            }
                        } else {
                            if self.supports_simple_auth
                                && login.eq(CONFIG.login())
                                && passcode.eq(CONFIG.passcode())
                            {
                                {
                                    let mut session = context.session.write().unwrap();
                                    session.set_login(login);
                                    session.set_flag(FLAG_ADMIN);
                                }
                                let connected_message =
                                    self.get_response_connected(message, context.session.clone());
                                context
                                    .session
                                    .read()
                                    .unwrap()
                                    .send_message(connected_message);

                                return Ok(HANDLED);
                            }
                            if self.supports_user_auth {
                                let sec = USER.check_passcode(login, passcode);
                                if sec.0 {
                                    {
                                        let mut session = context.session.write().unwrap();
                                        session.set_login(login);
                                        if sec.1 {
                                            session.set_flag(FLAG_ADMIN);
                                        }
                                    }
                                    let connected_message = self
                                        .get_response_connected(message, context.session.clone());
                                    context
                                        .session
                                        .read()
                                        .unwrap()
                                        .send_message(connected_message);

                                    return Ok(HANDLED);
                                }
                            }
                        }
                    }
                    // no login/passcode
                    if context.is_admin() {
                        // port 61616 implied admin privs
                        {
                            let mut session = context.session.write().unwrap();
                            if !session.is_authenticated() {
                                session.set_login("admin");
                            }
                        }
                        let connected_message =
                            self.get_response_connected(message, context.session.clone());
                        context
                            .session
                            .read()
                            .unwrap()
                            .send_message(connected_message);
                        return Ok(HANDLED);
                    }
                    return Err(FilterError {
                        reply_message: some_string("auth err"),
                        log_message: some_string("auth err"),
                        fatal: true,
                    });
                } else {
                    let connected_message =
                        self.get_response_connected(message, context.session.clone());
                    context
                        .session
                        .read()
                        .unwrap()
                        .send_message(connected_message);
                    return Ok(HANDLED);
                }
            }
            _ => {}
        }

        Ok(CONTINUE)
    }
}
