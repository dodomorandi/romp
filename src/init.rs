//! Contains global objects started during bootstrap, this is principally the `CONFIG` which provides
//! access to data in `romp.toml`, `SERVER` which contains the destinations (queues and topics)
//! and `USER` which contains configured users and functions to authenticate users.

use std::fs;

use crate::config;
use crate::config::user::UserConfig;
use crate::config::ServerConfig;
use crate::config::CONFIG_FILE;
use crate::workflow::console::ngin::Ngin;
use crate::workflow::destination::server::DestinationServer;

// Static initialization routines
// such statics are not native rust and IDE does not support this.
lazy_static! {
    pub static ref CONFIG: ServerConfig = {
        config::log::init_log4rs();

        let mut conf_path = String::from("./conf");
        if let Ok(var) = std::env::var("ROMP_CONF") {
            conf_path = String::from(var.as_str());
        }

        conf_path.push('/');
        conf_path.push_str(CONFIG_FILE);

        match fs::read_to_string(conf_path) {
            Ok(conf_data) => {
                let mut conf: ServerConfig = toml::from_str(&conf_data).unwrap();
                conf.init();
                conf
            }
            Err(e) => {
                eprintln!("loading config failed: {} {}", CONFIG_FILE, e);
                panic!();
            }
        }
    };
    pub static ref SERVER: DestinationServer = {
        let mut server = DestinationServer::new();
        server.init(&CONFIG);
        server
    };
    pub static ref USER: UserConfig = {
        let mut user = UserConfig::new();
        if let Some(user_file) = &CONFIG.user_file {
            let mut user_path: String;
            if user_file.starts_with('/') {
                user_path = user_file.to_string();
            } else {
                let mut conf_path = String::from("./conf");
                if let Ok(var) = std::env::var("ROMP_CONF") {
                    conf_path = var;
                }
                user_path = conf_path;
                user_path.push('/');
                user_path.push_str(user_file);
            }
            user.init(&user_path);
        }
        user
    };
    pub static ref NGIN: Ngin = Ngin::new();
}

// N.B. if code does not reference the refs they are never initialized.
