//! STOMP message representation and utils for generating responses and serialing STOMP messages.

// looks for message/response.rs
pub mod request;
pub mod response;
pub mod serializer;
pub mod stomp_message;
