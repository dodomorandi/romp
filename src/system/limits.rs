use rlimit::{Resource, Rlim};

use std::{convert::TryInto, io};

/// Set system limits on number of "open files", two files are used
/// per TCP socket in Linux
pub fn increase_rlimit_nofile(limit: u64) -> io::Result<Rlim> {
    let target = Rlim::from_usize(limit.try_into().expect("limit too large for system"));
    Resource::NOFILE.set(target, target)?;

    Ok(target)
}
