#!/bin/bash -e
#
# release a version
#
cd $(dirname $0)/..
. version

echo "releasing romp $VERSION"
echo
echo "continue? y/n"

read ok
if [[ $ok == "y" ]]
then
  VCS=bzr mng upload "version $VERSION"
  VCS=git mng upload "version $VERSION"
  cargo publish
fi
