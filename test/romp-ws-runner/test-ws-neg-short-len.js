"use strict";

const net = require('net');
const assert = require('assert');

/**
 * Test what happens when len field is too short, nothing much expected, just make sure we dont crash
 * on typical protocol violation attacks.
 */
let client = net.createConnection({host:"localhost", port: 61613}, () => {
	client.write("GET /xtomp HTTP/1.1\n" +
		"host: localhost\n" +
		"connection: upgrade\n" +
		"upgrade: websocket\n" +
		"origin: http://localhost\n" +
		"Sec-WebSocket-Version: 13\n" +
		"Sec-WebSocket-Protocol: stomp\n" +
		"Sec-WebSocket-Key: lCMmdU5K24A2vDkOYrqRLQ==\n\n");
});
client.on('data', (data) => {
	let string = data.toString();
	if ( data.indexOf('HTTP') === 0 ) {
		let status = string.split('\n')[0].trim();
		assert.equal(status, "HTTP/1.1 101 Switching Protocols");
		let buf = new Buffer(36);
		buf.writeUInt8(0x81,0);
		buf.writeUInt8(0x82, 1); // wrong len = 2
		buf.writeUInt32BE(0x00, 2); // noop mask
		buf.write("SUBSCRIBE\ndestination:memtop-a\n\n\0", 6);
		client.write(buf);
		// server should read "SU" then expect another frame
		// when it gets junk it should close
		setTimeout(() => {
			// waiting for data would be a bug
			client.end();
			process.exit(1);
		}, 500);
	}
	else {
	}
});
client.on('error', (data) => {
	// expected
	process.exit(0);
});
client.on('close', (data) => {
	// expected
	process.exit(0);
});
