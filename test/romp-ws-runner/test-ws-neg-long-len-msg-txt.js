"use strict";

const net = require('net');
const assert = require('assert');

/**
 * Test what happens when len field is too long in a SEND frame with a text message (\0 terminated).
 */
let client = net.createConnection({host:"localhost", port: 61613}, () => {
	client.write("GET /xtomp HTTP/1.1\n" +
		"host: localhost\n" +
		"connection: upgrade\n" +
		"upgrade: websocket\n" +
		"origin: http://localhost\n" +
		"Sec-WebSocket-Version: 13\n" +
		"Sec-WebSocket-Protocol: stomp\n" +
		"Sec-WebSocket-Key: lCMmdU5K24A2vDkOYrqRLQ==\n\n");
});
client.on('data', (data) => {
	let string = data.toString();
	if ( data.indexOf('HTTP') === 0 ) {
		let status = string.split('\n')[0].trim();
		assert.equal(status, "HTTP/1.1 101 Switching Protocols");
		let buf = new Buffer(48);
		buf.writeUInt8(0x81, 0);
		buf.writeUInt8(0xAA, 1); // right length
		buf.writeUInt32BE(0x00, 2); // noop mask
		buf.write("CONNECT\nlogin: xtomp\npasscode: passcode\n\n\0", 6); // len 42
		client.write(buf);
	}
	else {

		let buf = new Buffer(47);
		buf.writeUInt8(0x81, 0);
		buf.writeUInt8(0xF8, 1); // wrong len (120)
		buf.writeUInt32BE(0x00, 2); // noop mask
		buf.write("SEND\ndestination:memtop-a\n\n{\"test\":true}\0", 6); // len 41
		client.write(buf);

		// romp errors immediately

	}
});
client.on('error', (data) => {
	// console.log("expected: server closed conn");
	process.exit(0);
});
client.on('close', (data) => {
	// console.log("error server closed conn");
	process.exit(0);
});
