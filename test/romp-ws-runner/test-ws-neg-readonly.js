"use strict";

const stompFrame = require('stomp-frame');
const stomp = require('stomp');
const WebSocketClient = require('websocket').client;

function sendConnect(connection) {
	if ( connection.connected ) {
		connection.sendUTF("CONNECT\nlogin:xtomp\npasscode:passcode\n\n\0");
	}
}

function sendSubscribe(connection, q) {
	connection.sendUTF("SUBSCRIBE\n" +
		"destination:" + q + "\n" +
		"receipt:sub\n" +
		"id:1\n" +
		"\n\0");
}

function sendMessage(connection, q) {
	connection.sendUTF("SEND\n" +
		"destination:" + q + "\n" +
		"receipt:msg\n" +
		"\n" +
		"should be denied\n\0");
}

function sendDisconnect(connection, q) {
	connection.sendUTF("DISCONNECT\n\n\0");
}

const client = new WebSocketClient();

client.on('connectFailed', function(error) {
	console.log('Connect Error: ' + error.toString());
});

client.on('connect', function(connection) {
	// console.log('WebSocket Client Connected');
	connection.on('error', function(error) {
		//console.log("Connection Error: " + error.toString());
		process.exit(1);
	});
	connection.on('close', function() {
		//console.log('stomp Connection Closed');
	});
	connection.on('message', function(message) {
		
		let msg = "" + message.utf8Data;
		let frame = stompFrame.parseStompMessage(msg);
		if ( frame.command === "CONNECTED" ) {
			//console.log("CONNECTED");
			sendSubscribe(connection, "memtop-a");
			sendMessage(connection, "memtop-b");
		}
		else if ( frame.command === "RECEIPT" && frame.headers['receipt-id'] === "sub" ) {
			//console.log("RECEIPT: sub");
		}
		else if ( frame.command === "ERROR" && frame.headers.message === "blocked" ) {
			//console.log("ERROR: blocked");
			sendDisconnect(connection);
			connection.close();
			process.exit(0);
		}
		else if ( frame.command === "RECEIPT" && frame.headers['receipt-id'] === "msg" ) {
			console.log("RECEIPT: msg");
			console.log("ERROR should have been blocked");
			sendDisconnect(connection);
			connection.close();
			process.exit(1);
		}
		else {
			console.log("Received: '" + msg + "'");
			console.dir(frame);
		}
	});
	sendConnect(connection);
});

client.connect('ws://localhost:8080/xtomp', 'stomp');


process.on('SIGINT', () => {
	process.exit(0);
});
