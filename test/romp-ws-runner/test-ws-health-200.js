"use strict";


const net = require('net');
const assert = require('assert');

let client = net.createConnection({host:"localhost", port: 61613}, () => {
	client.write("GET /romp/health_check HTTP/1.1\nHost:localhost\n\n");
});
client.on('data', (data) => {
	let status = data.toString().split('\n')[0].trim();
	assert.equal(status, "HTTP/1.1 200 OK");
	client.end();
	process.exit(0);
});
