"use strict";


const net = require('net');
const assert = require('assert');

/**
 * Test setup via WS and CONNECT
 */
let client = net.createConnection({host:"localhost", port: 61613}, () => {
	client.write("GET /xtomp HTTP/1.1\n" +
		"host: localhost\n" +
		"connection: upgrade\n" +
		"upgrade: websocket\n" +
		"origin: http://localhost\n" +
		"Sec-WebSocket-Version: 13\n" +
		"Sec-WebSocket-Protocol: stomp\n" +
		"Sec-WebSocket-Key: lCMmdU5K24A2vDkOYrqRLQ==\n\n");
});
client.on('data', (data) => {
	let string = data.toString();
	if ( data.indexOf('HTTP') === 0 ) {
		let status = string.split('\n')[0].trim();
		assert.equal(status, "HTTP/1.1 101 Switching Protocols");

		let buf = new Buffer(16);
		buf.writeUInt8(0x81, 0);
		buf.writeUInt8(0x8a, 1);   // len = 10
		buf.writeUInt32BE(0x0, 2); // noop mask
		buf.write("CONNECT\n\n\0", 6);
		client.write(buf);
		setTimeout(() => {
			client.end();
			process.exit(1);
		}, 500);
	}
	else {
		client.end();
		process.exit(0);
	}
});
