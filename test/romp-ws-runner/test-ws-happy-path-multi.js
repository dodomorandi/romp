"use strict";

/**
 * Send multiple messages on the same socket,
 */

const stompFrame = require('stomp-frame');
const stomp = require('stomp');
const WebSocketClient = require('websocket').client;

const MESSAGE_COUNT = 10;
let messagesReceived = 0;

function sendConnect(connection) {
	if ( connection.connected ) {
		connection.sendUTF("CONNECT\nlogin:xtomp\npasscode:passcode\n\n\0");
	}
}
function sendSubscribe(connection, q) {
	//console.log("sending sub")
	connection.sendUTF("SUBSCRIBE\n" +
		"destination:" + q + "\n" +
		"receipt:sub\n" +
		"id:1\n" +
		"\n\0");
}
function sendDisconnect(connection, q) {
	connection.sendUTF("DISCONNECT\n\n\0");
}

const client = new WebSocketClient();

client.on('connectFailed', function(error) {
	console.log('Connect Error: ' + error.toString());
});

client.on('connect', function(connection) {
	//console.log('WebSocket Client Connected');
	connection.on('error', function(error) {
		//console.log("Connection Error: " + error.toString());
		process.exit(1);
	});
	connection.on('close', function() {
		console.log('stomp Connection Closed');
	});
	connection.on('message', function(message) {

		let msg = "" + message.utf8Data;
		let frame = stompFrame.parseStompMessage(msg);
		if ( frame.command === "CONNECTED" ) {
			//console.log("CONNECTED");
			sendSubscribe(connection, "memtop-a");
		}
		else if ( frame.command === "RECEIPT" && frame.headers['receipt-id'] === "sub" ) {
			//console.log("RECEIPT");
			stompPublish();

		}
		else if ( frame.command === "MESSAGE" ) {
			//console.log("MESSAGE");
			sendDisconnect(connection);
			// ALL GOOD message received ia websockets
			process.exit(0);
		}
		else {
			console.log("Received: '" + msg + "'");
			console.dir(frame);
		}
	});
	sendConnect(connection);
});

client.connect('ws://localhost:8080/xtomp', 'stomp');

function stompPublish() {
	const num = process.argv[2];

	const stompArgs = {
		port: 61613,
		host: 'localhost',
		debug: false,
		login: 'publisher',
		passcode: 'passcode',
	};

	const stompClient = new stomp.Stomp(stompArgs);

	stompClient.connect();

	stompClient.on('connected', () => {
		//console.log("connected");
		for (let i = 0 ; i < MESSAGE_COUNT ; i++) {
			stompClient.send({
				'destination': 'memtop-a',
				'body': "Testing\n" + new Date() + "\n:" + i,
				'foo': '' + Math.random(),
				'receipt' : 'send'
			});
		}
	});

	stompClient.on('receipt', (id) => {
		// console.log("receipt received: " + id);
		if (id === 'send') {
			messagesReceived++;
		}
		if ( messagesReceived == MESSAGE_COUNT) {
			stompClient.disconnect();
			process.exit(0);
		}
	});

	stompClient.on('error', (errorframe) => {
		console.log(errorframe.body);
		stompClient.disconnect();
		process.exit(1);
	});

	process.on('SIGINT', () => {
		stompClient.disconnect();
		process.exit(0);
	});

}
