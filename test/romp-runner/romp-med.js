var sys = require('util');
var stomp = require('stomp');

var stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: "xtomp",
    passcode: "passcode",
};

/**
 * Medium sized message but > 4000 which is client buffer size
 */
var client = new stomp.Stomp(stomp_args);


client.on("connected", function() {
    var ten = "1234567890";
    var testMessage = "";
    for (var i = 0 ; i < 401 ; i++) testMessage += ten;

    client.send({
        destination : "memtop-a",
        receipt : "send",
        body : testMessage,
    });
});

client.on("receipt", function(id) {
    if ( id === "send" ) {
        process.exit(0);
    }
});

client.on("error", function(error_frame) {
    if ( error_frame.headers.message === "msg flup" ) {
        client.disconnect();
        process.exit(0);
    }
    else {
        console.error("unexpected error:" + error_frame.headers.message);
        process.exit(1);
    }
});

client.connect();

process.on("SIGINT", function() {
    client.disconnect();
});

setTimeout(function() {
    console.error("test timeout");
    process.exit(1);
}, 10000);
