#!/bin/bash

node soak/subscribe.js &
node soak/publish.js &
node soak/subscribe-rand.js &
node soak/publish-rand.js &

while true
do
	sleep 60
done

kill -9 %1 %2 %3 %4

