"use strict";

const sys = require('util');
const stomp = require('stomp');
const crypto = require('crypto');

// code copied from xtomp_auth, requires setting up sha auth in romp.toml

var storedSecret = "XIxoIl6ngolYKQOrXpunRLCMWxR6O0lDI+HycNN4Ffo=";

function getLogin(algo, username, password) {

    let secret;

    if ( password ) secret = password;
    else secret = storedSecret;

    let rv = {
        login : username + " " + Math.floor(new Date().getTime() / 1000) + " " + Math.floor(Math.random() * Number.MAX_SAFE_INTEGER).toString(32),
    };
    rv.passcode = shaHash(algo, rv.login + secret);

    return rv;
};

function getLoginSha1(username, password) {
    return getLogin('sha1', username, password);
};

function getLoginSha256(username, password) {
    return getLogin('sha256', username, password);
};

function shaHash(algo, input) {
    return crypto.createHash(algo).update(input).digest('base64');
}

/**
 * Test connect with shared secret
 * To use sha256 set STOMP_AUTH_USE_256 0  in xtomp_auth.c and recompile
 */

let login = getLoginSha1("harry", "XIxoIl6ngolYKQOrXpunRLCMWxR6O0lDI+HycNN4Ffo=");
const stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: login.login,
    passcode: login.passcode,
};

const client = new stomp.Stomp(stomp_args);

client.connect();

client.on('connected', function() {
    client.disconnect();
});

client.on('error', function(error_frame) {
    console.error(error_frame);
    console.error(error_frame.body);
    client.disconnect();
    setTimeout( () => process.exit(1), 20);
});

process.on('SIGINT', function() {
    client.disconnect();
});
