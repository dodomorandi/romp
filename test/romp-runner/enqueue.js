#!/usr/bin/env node

// manually push a message to memq-1 and exit (not part of test suite)

var stomp = require('stomp');

var stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: 'publisher',
    passcode: 'passcode',
}

var client = new stomp.Stomp(stomp_args);

client.connect();

client.on('connected', function() {
    client.send({
        'destination': 'memq-1',
        'body': "Testing\n" + new Date() + "\n123",
        'foo': 'baa',
        'receipt' : 'send'
    });
});

client.on('receipt', function(id) {
    // console.log("receipt received: " + id);
    if (id === 'send') {
        client.disconnect();
    }
});

client.on('error', function(error_frame) {
    console.log(error_frame.body);
    client.disconnect();
});

process.on('SIGINT', function() {
    client.disconnect();
    process.exit(0);
});
