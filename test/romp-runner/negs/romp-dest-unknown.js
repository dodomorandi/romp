var sys = require('util');
var stomp = require('stomp');

var stomp_args = {
    port: 61613,
    host: 'localhost',
    debug: false,
    login: 'xtomp',
    passcode: 'passcode',
};

var client = new stomp.Stomp(stomp_args);

client.connect();

client.on('connected', function() {
    client.subscribe({
        destination: 'flipflop',
        ack: 'client',
        receipt: 'sub'
    });
});

client.on('receipt', function(message) {
    client.disconnect();
});

client.on('error', function(error_frame) {
    if ( error_frame.headers.message === "destination unknown" ) {
        //console.log("destination unknown");
        client.disconnect();
    }
    else {
        console.err("unexpected error:" + error_frame.headers.message);
        process.exit(1);
    }
});

process.on('SIGINT', function() {
    client.disconnect();
});
