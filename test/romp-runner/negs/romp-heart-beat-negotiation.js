var sys = require('util');
var stomp = require('stomp');

var stomp_args = {
    port: 61613,
    host: "localhost",
    debug: false,
    login: "xtomp",
    passcode: "passcode",
};


var doConnect = function(heartBeat, expect) {

    stomp_args["heart-beat"] = heartBeat;
    var client = new stomp.Stomp(stomp_args);

    client.on("connected", function(frame) {
        if ( frame.headers["heart-beat"] !== expect ) {
            console.error("expected " + expect + ", got " + frame.headers["heart-beat"] );
            process.exit(1);
        }
        client.disconnect();
    });
    client.on("error", function(frame) {
        console.log( frame.headers.message + " - " + heartBeat + " - " + expect);
        client.disconnect();
        process.exit(1);
    });
    client.connect();
};

// head-beat: what-i-can-do:what-i-would-like
doConnect("120000,120000", "120000,120000");
doConnect("0,0", "0,0");
doConnect("1000,120000", "120000,120000");
doConnect("120000,240000", "180000,120000"); // won't wait that long

var doNegConnect = function(heartBeat, expect) {

    stomp_args["heart-beat"] = heartBeat;
    var client = new stomp.Stomp(stomp_args);

    client.on("connected", function(frame) {
        if ( frame.headers["heart-beat"] ) {
            console.error("expected error, got " + frame.headers["heart-beat"] );
            process.exit(1);
        }
        client.disconnect();
    });
    client.on("error", function(frame) {
        if ( frame.headers.message === "ecg err" ) {
            client.disconnect();
            process.exit(0);
        }
        else {
            console.log("neg failed: " + frame.headers.message );
            client.disconnect();
            process.exit(1);
        }
    });
    client.connect();
};

doNegConnect("120000,1000");   // won't ping that often
