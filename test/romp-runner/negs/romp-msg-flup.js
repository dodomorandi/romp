var sys = require('util');
var stomp = require('stomp');

var stomp_args = {
    port: 61613,
    host: "localhost",
    debug: false,
    login: "xtomp",
    passcode: "passcode",
};


var client = new stomp.Stomp(stomp_args);


client.on("connected", function() {
    var testMessage = "";
    for (var i = 0 ; i < 1024 * 1024 ; i++) testMessage += "0";
    testMessage += "0";
    client.send({
        destination : "memtop-a",
        receipt : "send",
        body : testMessage,
    });
});

client.on("receipt", function() {
    console.error("unexpected error");
    process.exit(1);
});

client.on("error", function(error_frame) {
    if ( error_frame.headers.message === "msg flup" ) {
        client.disconnect();
        process.exit(0);
    }
    else {
        console.error("unexpected error:" + error_frame.headers.message);
        process.exit(1);
    }
});

client.connect();

process.on("SIGINT", function() {
    client.disconnect();
});

setTimeout(function() {
    console.error("test timeout");
    process.exit(1);
}, 1000);
