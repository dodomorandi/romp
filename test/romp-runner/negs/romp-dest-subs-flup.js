var sys = require('util');
var stomp = require('stomp');

var stomp_args = {
    port: 61613,
    host: "localhost",
    debug: false,
    login: "xtomp",
    passcode: "passcode",
};

var clients = [];
var connected = 0;
var CLIENTS = 4;

var createClient = function () {
    var client = new stomp.Stomp(stomp_args);

    clients.push(client);
    
    client.connect();

    client.on("connected", function() {
        client.subscribe({
            destination: "3-flup",
            ack: "client",
            receipt: "sub"
        });
    });

    client.on("receipt", function(message) {
        connected++;
        setTimeout(function() {
            if ( connected != 3 ) {
                console.error("expected 3-flup to let 3 connections subscribe");
                process.exit(1);
            }
            client.disconnect();
        }, 100);
    });

    client.on("error", function(error_frame) {
        if ( error_frame.headers.message === "subs flup" ) {
            client.disconnect();
        }
        else {
            console.error("unexpected error:" + error_frame.headers.message);
            process.exit(1);
        }
    });
};

process.on("SIGINT", function() {
    for(var i = 0 ; i < CLIENTS ; i++) {
        clients[i].disconnect();
    }
});

for (var i = 0 ; i < CLIENTS ; i++ ) {
    createClient(i);
}
