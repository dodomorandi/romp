"use strict";

const sys = require('util');
const stomp = require('stomp');

const MAX_SUBS = 4;

const stomp_args = {
    port: 61613,
    host: "localhost",
    debug: false,
    login: "xtomp",
    passcode: "passcode",
};

const client = new stomp.Stomp(stomp_args);

client.connect();

client.on("connected", function() {
    for (let i = 0 ; i < MAX_SUBS + 1 ; i++) {
        client.subscribe({
            destination: "memtop-a",
            ack: "client",
            receipt: i
        });
    }
});

client.on("receipt", function(message) {
    if (parseInt(message) === MAX_SUBS) {
        console.error("allowed to oversubscribe, >" + MAX_SUBS);
        process.exit(1);
    }
});

client.on("error", function(error_frame) {
    if ( error_frame.headers.message === "subs flup" ) {
        client.disconnect();
    }
    else {
        console.error("unexpected error:" + error_frame.headers.message);
        process.exit(1);
    }
});

process.on("SIGINT", function() {
    client.disconnect();
});

