var sys = require('util');
var stomp = require('stomp');

var stomp_args = {
    port: 61613,
    host: "localhost",
    debug: false,
    login: "xtomp",
    passcode: "passcode"
};

var client = new stomp.Stomp(stomp_args);

client.connect();

client.on("connected", function() {
    var hdrs = {
        destination: "memtop-a",
        ack: "client",
        long:   "12345678901234567890" +
                "12345678901234567890" +
                "12345678901234567890" +
                "12345678901234567890" +
                "12345678901234567890" +
                "12345678901234567890" +
                "12345678901234567890" +
                "12345678901234567890" +
                "12345678901234567890" +
                "1234567890123456", // + "long:"
        receipt: "pub"
    };

    client.send(hdrs);
});

client.on("receipt", function(message) {
    setTimeout(function() {
        console.error("expected hdrs flup");
        process.exit(1);
        client.disconnect();
    }, 100);
});

client.on("error", function(error_frame) {
    if ( error_frame.headers.message === "hdr flup" ) {
        client.disconnect();
    }
    else {
        console.error("unexpected error:" + error_frame.headers.message);
        process.exit(1);
    }
});


process.on("SIGINT", function() {
    client.disconnect();
});
