var sys = require('util');
var stomp = require('stomp');

/*
 * Test subscribing and unsubscribing.
 */
var stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: 'xtomp',
    passcode: 'passcode',
};

var client = new stomp.Stomp(stomp_args);

client.on('connected', function() {
    client.subscribe({
        destination: 'memtop-a',
        id : '1',
        ack: 'client',
        receipt: 'sub' 
    });
});

client.on('receipt', function(id) {
    if (id === 'sub') {
        client.unsubscribe({
            destination: 'memtop-a',
            id : '1',
            ack: 'client',
            receipt: 'unsub' 
        });
    }
    else if (id === 'unsub') {
        client.disconnect();
    }
    else {
        console.error("unexpected receipt");
    }
});

client.on('message', function(message) {
    console.log("headers: " + sys.inspect(message.headers));
    console.log("body: " + message.body);
    client.ack(message.headers['message-id']);
    messages++;
});

client.on('error', function(error_frame) {
    console.log("error");
    console.log("headers: " + sys.inspect(error_frame.headers));
    console.log("body: " + error_frame.body);
    client.disconnect();
});

process.on('SIGINT', function() {
    console.log('\nconsumed ' + messages + ' messages');
    client.disconnect();
});

client.connect();