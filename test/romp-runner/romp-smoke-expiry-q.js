"use strict";
const sys = require('util');
const stomp = require('stomp');

/*
 * Test simple send and receive use case from q-expiry-a.
 */

const stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: "xtomp",
    passcode: "passcode",
};

const testMessage =  "Testing\n" + process.env.TEST_DATE + "\n123";

const errorHandler = (error_frame) => {
    console.log("error");
    console.log("headers: " + sys.inspect(error_frame.headers));
    console.log("body: " + error_frame.body);
    publisher.disconnect();
    subscriber.disconnect();
};



// SUBSCRIBER - start

stomp_args.login = "sub1";
const subscriber = new stomp.Stomp(stomp_args);

subscriber.on("connected", () => {
    subscriber.subscribe({
        destination: "expiry-q",
        id : "1",
        ack: "client",
        receipt: "sub" 
    });
});

subscriber.on("receipt", (id) => {
    if (id === "sub") {
        subscriber.subscribed = true;
    }
    else if (id === "unsub") {
        // console.log("OK");
        subscriber.disconnect();
    }
    else console.error("unexpected receipt");
});

subscriber.on("message", (message) => {
    if (testMessage !== "" + message.body) {
        console.error("message different");
        console.error("send:" + testMessage);
        console.error("recv:" + message.body);
    }
    subscriber.ack(message.headers["message-id"]);
    subscriber.unsubscribe({
        destination: "expiry-q",
        id : "1",
        receipt: "unsub" 
    });
});

subscriber.on("error", errorHandler);

setTimeout( () => subscriber.connect(), 10);

// SUBSCRIBER - end

// PUBLISHER - start

stomp_args.login = "publisher";
const publisher = new stomp.Stomp(stomp_args);

publisher.on("connected", () => {
    publisher.send({
        "destination" : "expiry-q",
        "body" : testMessage,
        "receipt" : "send"
    });
});

publisher.on("receipt", (id) => {
    if (id === "send") {
        publisher.disconnect();
    }
    else console.error("unexpected receipt");
});

publisher.on("error", errorHandler);

publisher.connect();

// PUBLISHER - end

process.on("SIGINT", () => {
    publisher.disconnect();
    subscriber.disconnect();
    process.exit(0);
});


