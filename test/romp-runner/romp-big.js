var sys = require('util');
var stomp = require('stomp');

var stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: "xtomp",
    passcode: "passcode",
};


var client = new stomp.Stomp(stomp_args);

/*
 * send a big message 2^20 which is 1MB i.e. exactly max size
 */
client.on("connected", function() {
    var testMessage = "0";
    for (var i = 0 ; i < 20 ; i++) testMessage += testMessage;
    client.send({
        destination : "memtop-a",
        receipt : "send",
        body : testMessage,
    });
});

client.on("receipt", function(id) {
    if ( id === "send" ) {
        process.exit(0);
    }
    else {
        console.log("received receipt-id:" + id);
    }
});

client.on("error", function(error_frame) {
    if ( error_frame.headers.message === "msg flup" ) {
        client.disconnect();
        process.exit(1);
    }
    else {
        console.error("unexpected error:" + error_frame.headers.message);
        process.exit(1);
    }
});

client.connect();

process.on("SIGINT", function() {
    client.disconnect();
});

setTimeout(function() {
    console.error("test timeout");
    process.exit(1);
}, 10000);
