#!/usr/bin/env node

var stomp = require('stomp');

var num = process.argv[2];

var stomp_args = {
    port: 61613,
    host: 'localhost',
    debug: false,
    login: 'publisher',
    passcode: 'passcode',
    "heart-beat": "120000,120000",
}

var client = new stomp.Stomp(stomp_args);

client.connect();

function randomBody() {
    var data = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
    var msg = data;
    max = Math.random() * 10000;
    for (var i = 0 ; i < max ; i++ ) {
        msg += data;
    }
    data = "12345678901234567890123456789012345";
    var msg = data;
    max = Math.random() * 10000;
    for (var i = 0 ; i < max ; i++ ) {
        msg += data;
    }
    max = Math.random() * 1000;
    for (var i = 0 ; i < max ; i++ ) {
        msg += Math.floor(Math.random() * 1000000);
    }
    console.log(msg.length);
    return msg;
}

function doSend() {
    client.send({
        'destination': 'memtop-a',
        'body': randomBody(),
        'receipt' : 'send'
    });
}

client.on('connected', function() {
    client.sent = 0;
    doSend();
});

client.on('receipt', function(id) {
    client.sent++;
    setTimeout(doSend, 1000);
    //if ( client.sent % 1000 === 0) 
    console.log("sent: " + client.sent );
});

client.on('error', function(error_frame) {
    console.log(error_frame.headers);
    client.disconnect();
    setTimeout(function () {
        process.exit(1);
    }, 1000);
});

process.on('SIGINT', function() {
    client.disconnect();
    process.exit(0);
});
