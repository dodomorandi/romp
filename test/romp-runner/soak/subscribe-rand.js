var sys = require('util');
var stomp = require('stomp');

var stomp_args = {
    port: 61613,
    host: 'localhost',
    debug: false,
    login: 'subscriber',
    passcode: 'passcode',
    "heart-beat": "120000,120000",
};

var createClient = function() {

    stomp_args.localAddress = '127.0.0.' + (Math.floor(Math.random() * 250) + 1)
    var client = new stomp.Stomp(stomp_args);

    client.on('connected', function() {
        client.subscribe({
            destination: 'memtop-a',
            id : '1',
            ack: 'client',
            receipt: 'sub' 
        });
    });

    client.on('message', function(message) {
        client.ack(message.headers['message-id']);
    });

    client.on('error', function(error_frame) {
        console.log("error");
        console.log("headers: " + sys.inspect(error_frame.headers));
        client.disconnect();
    });

    client.connect();

    return client;
}

setInterval(function() {
    var client = createClient();
    setTimeout(function() {
        client.disconnect();
    }, Math.random() * 1000 * 10);
}, 10000);

process.on('SIGINT', function() {
    process.exit(0);
});
