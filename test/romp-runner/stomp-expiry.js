"use strict";
const sys = require('util');
const stomp = require('stomp');

/*
 * Test expiry, the subscriber should NOT receive the message.
 * Takes 120 secs to guarantee the test is successful (usually works after 60)
 * With pedantic expiry this should work immediately.
 */

const stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: "smokey",
    passcode: "passcode",
};

const testMessage =  "Testing\n" + process.env['TEST_DATE'] + "\n123";

const errorHandler = (error_frame) => {
    console.log("error");
    console.log("headers: " + sys.inspect(error_frame.headers));
    console.log("body: " + error_frame.body);
    publisher.disconnect();
    subscriber.disconnect();
};



// SUBSCRIBER - start

const subscriber = new stomp.Stomp(stomp_args);

subscriber.on("connected", () => {
    subscriber.subscribe({
        destination: "expiry-q",
        id : "1",
        ack: "client",
        receipt: "sub" 
    });
});

subscriber.on("receipt", (id) => {
    if (id === "sub") {
        subscriber.subscribed = true;
    }
    else console.error("unexpected receipt");
});

subscriber.on("message", (message) => {
    console.log("ERROR message received, should have expired");
    process.exit(1);
});

subscriber.on("error", errorHandler);

setTimeout( () => subscriber.connect(), 120000);
setTimeout( () => {
    if ( ! subscriber.subscribed) {
        console.log("ERROR subscriber did not subscribe");
        process.exit(1);
    }
    else {
        console.log("OK message not received, presume its expired");
        clearTimeout(ticker);
        subscriber.disconnect();
    }
}, 120000 + 100);

let tick = 0;
let ticker = setInterval( () => console.log("tick: " + (tick += 10) + "s"), 10000);
// SUBSCRIBER - end

// PUBLISHER - start

const publisher = new stomp.Stomp(stomp_args);

publisher.on("connected", () => {
    publisher.send({
        "destination" : "expiry-q",
        "body" : testMessage,
        "receipt" : "send"
    });
    console.log("Message sent, waiting 2 mins for it to expire...");
});

publisher.on("receipt", (id) => {
    if (id === "send") {
        publisher.disconnect();
    }
    else console.error("unexpected receipt");
});

publisher.on("error", errorHandler);

publisher.connect();

// PUBLISHER - end

process.on("SIGINT", () => {
    publisher.disconnect();
    subscriber.disconnect();
    process.exit(0);
});


