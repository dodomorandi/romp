var StompRaw = require('stomp-raw').StompRaw;


/*
 * If we do the CONNECT wrong we should get disconnected immediatly (with a syntax error)
 */
var stompRaw = new StompRaw();

stompRaw.on("connect", function() {
    stompRaw.write("WIBBLE\n\n\0");
});

stompRaw.on("frame", function(frame) {

    while (frame.charAt(0) === '\n' || frame.charAt(0) === '\r') {
        console.log("leading whitespace detected");
        frame = frame.substring(1);
    }

    if ( frame.startsWith("CONNECTED") ) {
        console.log("error");
        process.exit(1);
        stompRaw.end();
    }
    else if ( frame.startsWith("ERROR") ) {
        process.exit(0);
        stompRaw.end();
    }
});

stompRaw.connect();
