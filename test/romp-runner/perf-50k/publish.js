"use strict";

const stomp = require('stomp');


const stomp_args = {
    port: 61613,
    host: 'localhost',
    debug: false,
    login: 'guest',
    passcode: 'guest',
}


const start = new Date().getTime();

var msgCount = process.argv[2];
msgCount = msgCount || 100;
let count = 0;

let message = new Buffer(1024);
message.fill(".");
message = "" + message;

const client = new stomp.Stomp(stomp_args);

client.on('connected', () => {
    console.log("publisher connected");
    let i = 0;
    for ( ; i < msgCount ; i++ ) {
        client.send({
            'destination': 'memtop-a',
            'body': message,
            'receipt' : 'send'
        });
    }
    console.log('sent [count="' + i + '" d="' + (new Date().getTime() - start) + '"]');
});

client.on('receipt', (id) => {
    //console.log("receipt received: " + id);
    if (id === 'send' && ++count == msgCount) {
        console.log('confirmed [count="' + count + '" d="' + (new Date().getTime() - start) + '"]');
        cleanExit();
    }
    if (id === 'send' && count > msgCount) {
        console.log('OVERSEND [count="' + count + '" d="' + (new Date().getTime() - start) + '"]');
    }
});

client.on('error', function(error_frame) {
    console.log(error_frame.body);
    client.disconnect();
});

const cleanExit = function() {
    client.disconnect();
}

process.on('SIGINT', () => {
    cleanExit();
    process.exit(0);
});

client.connect();

