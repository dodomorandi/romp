var StompRaw = require('stomp-raw').StompRaw;

/*
 * Mimic telnet use which has problems.
 */
var stompRaw = new StompRaw({
    debug : false
});

var to = setTimeout(function() {
    process.exit(1);
}, 60000);

stompRaw.on("connect", function() {
    stompRaw.write("CONNECT\n");
    stompRaw.write("login:xtomp\n");
    stompRaw.write("passcode:passcode\n");
    stompRaw.write("\n");
    stompRaw.write("\0\n");
});

stompRaw.on("frame", function(frame) {

    while (frame.charAt(0) === '\n' || frame.charAt(0) === '\r') {
        console.log("leading whitespace detected");
        frame = frame.substring(1);
    }

    //console.log("frame:" + frame);
    var howSlow = 1;

    if ( frame.startsWith("CONNECTED") ) {
        //console.log("connected");
        stompRaw.write("SEND\n");
        stompRaw.write("destination:memtop-a\n");
        stompRaw.write("receipt:send\n");
        stompRaw.write("\n");
        setTimeout(function() {
            stompRaw.write("message\n");
        }, 100 * howSlow);
        setTimeout(function() {
            stompRaw.write("comes\n");
        }, 200 * howSlow);
        setTimeout(function() {
            stompRaw.write("in chunks\n\0");
        }, 300 * howSlow);
    }
    else if ( frame.trim().startsWith("RECEIPT") ) {
        stompRaw.write("DISCONNECT\n\n\0");
        clearTimeout(to);
    }
    else {
        console.log("unexpected frame");
        stompRaw.end();
    }
});

stompRaw.connect();
