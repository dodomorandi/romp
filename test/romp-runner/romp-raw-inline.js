var StompRaw = require('stomp-raw').StompRaw;

// test multiple STOMP messages in the same network packet using stomp-raw module

var stompRaw = new StompRaw();

stompRaw.on("connect", function() {
    stompRaw.write("CONNECT\nlogin:xtomp\npasscode:passcode\n\n\0SUBSCRIBE\ndestination:memtop-a\nreceipt:sub\n\n\0");
});

stompRaw.on("frame", function(frame) {

    while (frame.charAt(0) === '\n' || frame.charAt(0) === '\r') {
        console.log("leading whitespace detected");
        frame = frame.substring(1);
    }

    console.log(frame);

    if ( frame.startsWith("CONNECTED") ) {
        //stompRaw.end();
    }
    else if ( frame.startsWith("RECEIPT") ) {
        stompRaw.end();
    }
    else {
        console.log("error");
        stompRaw.end();
    }
});

stompRaw.connect();
