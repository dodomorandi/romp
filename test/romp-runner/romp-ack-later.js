var sys = require('util');
var stomp = require('stomp');

/*
 * Send message to a subscriber who does not ack, reconnect should replay the same message which is then acked
 */
var stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: "xtomp",
    passcode: "passcode",
};

var destination = "ackme";
var testMessage =  "Dont forget!\n";

var errorHandler = function(error_frame) {
    console.log("error");
    console.log("headers: " + sys.inspect(error_frame.headers));
    console.log("body: " + error_frame.body);
    setTimeout(function() {
        publisher.disconnect();
        subscriber.disconnect();
        process.exit(0);
    }, 100);
};

stomp_args.login = "sub1";
var subscriber = new stomp.Stomp(stomp_args);
subscriber.pass = 0;
stomp_args.login = "publisher";
var publisher = new stomp.Stomp(stomp_args);

// SUBSCRIBER - start

subscriber.on("connected", function() {
    subscriber.subscribe({
        destination: destination,
        id : "1",
        ack: "client",
        receipt: "sub"
    });
});

subscriber.on("receipt", function(id) {
    if (id === "sub") {
        subscriber.subscribed = 1;
    }
    else if (id === "unsub") {
        subscriber.subscribe({
            destination: destination,
            id : "2",
            ack: "client",
            receipt: "sub"
        });
    }
    else console.error("unexpected receipt " + id);
});

subscriber.on("message", function(message) {
    if (testMessage !== "" + message.body) {
        console.error("wrong msg");
        console.error("want:" + testMessage);
        console.error("got:" + message.body);
        errorHandler({});
    }
    else {
        subscriber.pass++;
        //console.log("on message " + subscriber.pass);
        // first time we do not ACK the message, and we re-subscribe
        if (subscriber.pass === 1) {
            subscriber.unsubscribe({
                destination: destination,
                id : "1",
                receipt: "unsub"
            });
        }
        // second time we ack the message, and resubscribe
        else if (subscriber.pass === 2) {
            subscriber.ack(message.headers["message-id"]);
            //console.log("ack message");
            setTimeout( () => {
                subscriber.unsubscribe({
                    destination: destination,
                    id : "2",
                    receipt: "unsub"
                });
            }, 50);

        }
        // if it arrives a third time its an error (we ACKed it)
        else if (subscriber.pass > 2) {
            console.log("ACKed message hanging around: " + message.headers["message-id"]);
            console.log("got:" + message.body);
        }
    }
});

subscriber.on("error", errorHandler);

// SUBSCRIBER1 - end

// PUBLISHER - start

var publisher = new stomp.Stomp(stomp_args);

publisher.on("connected", function() {

    if ( subscriber.subscribed === 1 ) {
        publisher.send({
            "destination" : destination,
            "body" : testMessage,
            "receipt" : "send"
        });
    }
    else {
        setTimeout(function() {
            publisher.send({
                "destination" : destination,
                "body" : testMessage,
                "receipt" : "send"
            });
        }, 100);
    }

});

publisher.on("receipt", function(id) {
    if (id === "send") {
        publisher.disconnect();
    }
});

publisher.on("error", errorHandler);

// PUBLISHER - end

process.on("SIGINT", function() {
    publisher.disconnect();
    subscriber.disconnect();
});


subscriber.connect();
publisher.connect();

setTimeout(function() {
    //console.log("timeout: expected");
    publisher.disconnect();
    subscriber.disconnect();
    if (subscriber.pass === 2) {
        process.exit(0);
    } else {
        process.exit(1);
    }
}, 1000);
