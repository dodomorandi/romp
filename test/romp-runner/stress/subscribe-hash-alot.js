"use strict";

const sys = require('util');
const stomp = require('stomp');

const stomp_args = {
    port: 61613,
    host: 'localhost',
    debug: false,
    login: 'subscriber',
    passcode: 'passcode',
    "heart-beat": "120000,120000",
};

var connected = 0;
/*
 * Subscribe and kill the TCP connection a repeatedly (every 100ms)
 */
var messages = 0;

const GROUPS = [];
GROUPS.push("zero");
GROUPS.push("one");
GROUPS.push("two");
GROUPS.push("three");
GROUPS.push("four");
GROUPS.push("five");
GROUPS.push("six");
GROUPS.push("seven");
GROUPS.push("eight");
GROUPS.push("nine");

const createClient = function() {

    stomp_args.localAddress = '127.0.0.' + (Math.floor(Math.random() * 250) + 1)
    let client = new stomp.Stomp(stomp_args);

    client.on('connected', function() {
        client.subscribe({
            destination: 'memtop-f',
            filter: "grp=" + GROUPS[Math.floor(Math.random() * 10)],
            id : '1',
            ack: 'client',
            receipt: 'sub' 
        });
        connected++;
        if ( connected % 100 == 0 ) console.log("connected " + connected);
    });

    client.on('message', function(message) {
        if ( client.ended ) {
            client.disconnect();
            return;
        }
        try {
            client.ack(message.headers['message-id']);
            messages++;
            if ( messages % 100 == 0 ) console.log("messages received " + messages);
        } catch (err) {
            // write after end normal in this test
        }
    });

    client.on('error', function(error_frame) {
        //console.log("headers: " + sys.inspect(error_frame.headers));
        client.disconnect();
    });

    client.connect();

    return client;
}

setInterval(function() {
    let client = createClient();
    setTimeout(function() {
        client.ended = true;
        client.socket.end();
    }, 100 + (Math.random() * 100));
}, 100);

process.on('SIGINT', function() {
    process.exit(0);
});
