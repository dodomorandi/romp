var StompRaw = require('stomp-raw').StompRaw;
var stompRaw = new StompRaw();

stompRaw.on("connect", function() {
    stompRaw.write("CONNECT\n\n\0");
    setTimeout(function() {
        stompRaw.write("\n\n\0");
    }, 10);
});

stompRaw.on("frame", function(frame) {
    if ( frame.startsWith("CONNECTED") ) {
    }
    else if ( frame.startsWith("ERROR") && frame.indexOf("message:syntax") > 0 ) {
        process.exit(0);
    }
    else {
        process.exit(1);
    }
});

stompRaw.connect();
setTimeout(function() {
    stompRaw.end();
}, 100);
