var stomp = require('stomp');
var client = new stomp.Stomp({});

/*
 * Test sending and receiveing a zero byte message
 */

client.on('connected', function() {
    client.subscribe({
        destination: 'memtop-a'
    });
});

client.on('message', function(message) {
    if ( message.headers["content-length"] == "0" ) {
        process.exit(0);
    }
    else {
        console.log("expected zero byte message");
        process.exit(1);
    }
});

client.on('error', function(error_frame) {
    console.log("error");
    process.exit(1);
});

client.connect();



var StompRaw = require('stomp-raw').StompRaw;
var stompRaw = new StompRaw();

stompRaw.on("connect", function() {
    stompRaw.write(
        "CONNECT\n" +
        "\n\0");

    setTimeout(function () {
        stompRaw.write(
            "SEND\n" +
            "destin"
        );
    }, 10);

    setTimeout(function () {
        stompRaw.write(
            "ation:memtop-a\n" +
            "\n\0"
        );
    }, 20);
});

stompRaw.on("frame", function(frame) {

    if ( frame.startsWith("CONNECTED") ) {
    }
    else if ( frame.startsWith("ERROR") ) {
        console.log("server reported error");
        process.exit(1);
    }
    else if ( frame.startsWith("RECEIPT") ) {
        process.exit(1);
    }
    else {
        process.exit(1);
    }
});

stompRaw.connect();
setTimeout(function() {
    stompRaw.end();
}, 100);
