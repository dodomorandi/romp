var stomp = require('stomp');
var client = new stomp.Stomp({});

/*
 * Test sending invalid header value \0
 */

client.on('connected', function() {
    client.subscribe({
        destination: 'memtop-a'
    });
});

client.on('message', function(message) {
    console.dir(message);
    console.log("expected no message");
    process.exit(1);
});

client.on('error', function(error_frame) {
    console.log("error");
    process.exit(1);
});

client.connect();



var StompRaw = require('stomp-raw').StompRaw;
var stompRaw = new StompRaw();

stompRaw.on("connect", function() {
    stompRaw.write(
        "CONNECT\n" +
        "\n\0");

    setTimeout(function () {
        stompRaw.write(
            "SEND\n" +
            "destin"
        );
    }, 10);

    setTimeout(function () {
        stompRaw.write(
            "ation:memtop-a\n" +
            "receipt:send\n" +
            "foo:baa\n" +
            "quxx:\0\n" +
            "\n1\0"
        );
    }, 20);
});

stompRaw.on("frame", function(frame) {

    if ( frame.startsWith("CONNECTED") ) {
    }
    else if ( frame.startsWith("ERROR") && frame.indexOf("message:syntax") > 0 ) {
        process.exit(0);
    }
    else if ( frame.startsWith("RECEIPT") ) {
        console.log("expected syntax error");
        process.exit(1);
    }
    else {
        process.exit(1);
    }
});

stompRaw.connect();
setTimeout(function() {
    stompRaw.end();
}, 100);
