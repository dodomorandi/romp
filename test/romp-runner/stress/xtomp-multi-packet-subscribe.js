var StompRaw = require('stomp-raw').StompRaw;
var stompRaw = new StompRaw();

stompRaw.on("connect", function() {
    stompRaw.write(
        "CONNECT\n" +
        "\n\0");

    setTimeout(function () {
        stompRaw.write(
            "SUBSCRIBE\n" +
            "destin"
        );
    }, 10);

    setTimeout(function () {
        stompRaw.write(
            "ation:memtop-a\n" +
            "receipt:oioi\n" +
            "\n\0"
        );
    }, 20);
});

stompRaw.on("frame", function(frame) {

    if ( frame.startsWith("CONNECTED") ) {
    }
    else if ( frame.startsWith("ERROR") ) {
        console.log("server reported error");
        process.exit(1);
    }
    else if ( frame.startsWith("RECEIPT") ) {
        process.exit(0);
    }
    else {
        process.exit(1);
    }
});

stompRaw.connect();
setTimeout(function() {
    stompRaw.end();
}, 100);
