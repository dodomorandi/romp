var sys = require('util');
var stomp = require('stomp');

/* 
 * memq test to ignore /queue/ prefixes since this kind af appears in the STOMP reference as alink.
 */

var stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: "memq",
    passcode: "passcode",
};

var destination = "/queue/memq";

var testMessage =  "Testing01\n";

var errorHandler = function(error_frame) {
    console.log("error");
    console.log("headers: " + sys.inspect(error_frame.headers));
    console.log("body: " + error_frame.body);
    setTimeout(function() {
        publisher.disconnect();
        subscriber.disconnect();
        process.exit(1);
    }, 2000);
};

stomp_args.login = "sub1";
var subscriber = new stomp.Stomp(stomp_args);
stomp_args.login = "publisher";
var publisher = new stomp.Stomp(stomp_args);

// SUBSCRIBER - start

subscriber.on("connected", function() {
    subscriber.subscribe({
        destination: destination,
        id : "1",
        ack: "client",
        receipt: "sub1"
    });
});

subscriber.on("receipt", function(id) {
    if (id === "sub1") {
        subscriber.subscribed = 1;
    }
    else if (id === "unsub1") {
        subscriber.disconnect();
    }
    else console.error("sub1 unexpected receipt " + id);
});

subscriber.on("message", function(message) {
    if (testMessage !== "" + message.body) {
        console.error("wrong msg");
        console.error("want:" + testMessage);
        console.error("got:" + message.body);
        errorHandler({});
    }
    else {
        subscriber.received = true;
        subscriber.ack(message.headers["message-id"]);
        subscriber.unsubscribe({
            destination: destination,
            id : "1",
            receipt: "unsub1"
        });
    }
});

subscriber.on("error", errorHandler);

// SUBSCRIBER - end

// PUBLISHER - start

var publisher = new stomp.Stomp(stomp_args);

publisher.on("connected", function() {

    if ( subscriber.subscribed === 1 ) {
        publisher.send({
            destination : destination,
            body : testMessage,
            receipt : "send"
        });
    }
    else {
        setTimeout(function() {
            publisher.send({
                destination : destination,
                body : testMessage,
                receipt : "send"
            });
        }, 100);
    }

});

publisher.on("receipt", function(id) {
    if (id === "send") {
        publisher.disconnect();
    }
});

publisher.on("error", errorHandler);

// PUBLISHER - end

process.on("SIGINT", function() {
    publisher.disconnect();
    subscriber.disconnect();
});


subscriber.connect();

setTimeout(function() {
    publisher.connect();
}, 10);

setTimeout(function() {
    if ( subscriber.received ) {
        // console.log("Subscribers got memq-1 message");
        process.exit(0);
    }
}, 200);

