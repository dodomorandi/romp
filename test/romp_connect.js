var sys = require('util');
var stomp = require('stomp');

/*
 * Test connect and nowt more.
 */
var stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: 'xtomp',
    passcode: 'passcode',
};

var client = new stomp.Stomp(stomp_args);

client.connect();

client.on('connected', function() {
    client.subscribe({
        destination: 'memtop-a',
        ack: 'client',
        receipt: 'sub'
    });
});

client.on('receipt', function(message) {
    client.disconnect();
    process.exit(0);
});

client.on('error', function(error_frame) {
    console.error("Error");
    console.error(error_frame.body);
    process.exit(1);
});

process.on('SIGINT', function() {
    client.disconnect();
    process.exit(2);
});
