#!/bin/bash

cd $(dirname $0)

run_test() {
	if [[ "$2" == "-v" ]]
	then
		echo $1
	fi
	node "$1"
	if [[ $? -ne 0 ]]
	then
		echo "error running:" "$1"
	fi
}

for test in $(ls romp_*.js)
do
	run_test $test $1
done

romp-runner/run-tests.sh $1
romp-ws-runner/run-tests.sh $1


