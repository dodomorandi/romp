
#
# server name
#
name = "romp"
#
# IP/host to bind server socket to (set to 0.0.0.0 for all interfaces)
#
bind_address = "127.0.0.1"
#
# TCP port to listen on
# N.B. if listen on 61616 all connections are admin,  80,81, 800 - 8099 are considered web ports (even if not using websockets)
#
ports = [61613, 8080, 61616]
#
# admin username
#
login = "admin"
#
# admin password
#
passcode = "PASSWORD"
#
# user to hashed password file
#
user_file = "romp.passwd"
#
#
#
pid_file = "/run/romp/romp.pid"

#
# security limits, by default limits are low
#

# Enables the /virt/admin/ virtual destination
enable_admin = true
max_message_size = 1048576
max_headers = 10
max_header_len = 200
# maximum amount of subscriptions on single TCP connection
max_subs = 4
# set the processes nofiles limit (only work if process runs as root)
rlimit_nofile = 1000000

#
# Shared secret used to generate auth tokens
# This should be a looooooooong complex password,
# if not, its printed to stdout cos you obviously don't care.
# `xtomp-auth` can generate these tokens
#
#secret = "XIxoIl6ngolYKQOrXpunRLCMWxR6O0lDI+HycNN4Ffo="
#
# How long auth tokes are valid for
#
#secret_timeout = 60000

#
# Size of each sessions's request buffer, all headers must fit
#
request_client_buffer = 4096
#
# Size of each client's response buffer, all output headers must fit
#
response_client_buffer = 200

#
# Hearbeat default parameters, see STOMP docs for details
#
heart_beat_read      = 120000
heart_beat_write_min = 60000
heart_beat_write_max = 180000

#
# Accepted Origins for websocket
#
websockets_origin = "http://xtomp.tp23.org http://tp23.org http://localhost http://localhost:8181"

[destinations]

	[destinations.memtop-a]
	name = "memtop-a"
	max_connections = 1000000
	max_messages = 100
	expiry = 120000
	max_message_size = 1048576
	stats = true

	[destinations.memtop-b]
	name = "memtop-b"
	max_connections = 1000000
	max_messages = 100
	expiry = 120000
	max_message_size = 1048576
	stats = true

	[destinations.memq-1]
	name = "memq-1"
	#
	# The minimum number of times a message must be delivered
	# to be considered sent.
	# When 0 messages are not stored if there are no subscribers.
	# When 1 the destination behaves like a queue and messages are buffed
	# until they are read. (or max_messages limit is reached)
	#
	min_delivery = 1
	max_connections = 2
	max_messages = 10
	stats = true

	[destinations.3-flup]
	name = "3-flup"
	max_connections = 3
	stats = true

	[destinations."/queue/memq"]
	name = "/queue/memq"
	min_delivery = 1
	max_connections = 2
	max_messages = 10
	stats = true

	[destinations.memtop-c]
	name = "memtop-c"
	stats = true
	#
	# SUBSCRIBE to this topic is blocked on websockets connections
	#
	web_read_block = true

	#
	# Q that is pretty much guaranted to expire message
	#
	[destinations.expiry-q]
	name = "expiry-q"
	#
	# messages expire after 10 msec
	#
	expiry = 10
	#
	# ensures expires with very short expiry time are not forwarded when expired
	# when false expiry happens every minutre or so, checks every message before sending it
	#
	pedantic_expires = true
	min_delivery = 1
	max_connections = 2
	stats = true

	[destinations.memtop-f]
	#
	# type: memory based pub/sub topic that allows filtering messages.
	#
	name = "memtop-f"
	filter = true
	stats = true

	[destinations.ackme]
	#
	# type: memory based pub/sub topic that allows filtering messages.
	#
	name = "ackme"
	min_delivery = 1
	stats = true
	auto_ack = false

	#
	# Magic topic, if this exists, statistics are published here
	#
	[destinations."/xtomp/stat"]
	name = "/xtomp/stat"
	max_connections = 5
	min_delivery = 0
	expiry = 60000
	#
	# this flag prevents read access (SUBSCRIBE) from the web
	#
	web_read_block = true
	#
	# this flag prevents write access (SEND) from the web
	#
	web_write_block = true
	#
	# this flag prevents write access (SEND) from anywhere the network
	#
	write_block = true
